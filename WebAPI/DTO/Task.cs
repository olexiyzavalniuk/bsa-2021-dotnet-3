﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTO
{
    public class Task
    {
            public int Id { get; set; }
            public int ProjectId { get; set; }
            public int PerformerId { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public TaskState State { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime? FinishedAt { get; set; }
    }
}
