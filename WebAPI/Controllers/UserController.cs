﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DAL;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public UserController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Route("create")]
        [HttpPost]
        public ActionResult<decimal> Create([FromBody] DTO.User user)
        {
            try
            {
                _unitOfWork.Users.Create(user);
                return Created("/api/user/create", user);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("read")]
        [HttpGet]
        public ActionResult<int> Read()
        {
            return Ok(_unitOfWork.Users.GetAll());
        }

        [Route("update")]
        [HttpPut]
        public ActionResult<int> Update([FromBody]  DTO.User user)
        {
            if (_unitOfWork.Users.Get(user.Id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Users.Update(user);
                return Ok(_unitOfWork.Users.Get(user.Id));
            }
            catch (ArgumentException e)
            {
                if (e.Message == "Vehicle not found")
                {
                    return NotFound(e.Message);
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public ActionResult<int> Delete(int id)
        {
            if (_unitOfWork.Users.Get(id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Users.Delete(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
