﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DAL;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public TaskController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Route("create")]
        [HttpPost]
        public ActionResult<decimal> Create([FromBody] DTO.Task task)
        {
            try
            {
                _unitOfWork.Tasks.Create(task);
                return Created("/api/user/create", task);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("read")]
        [HttpGet]
        public ActionResult<int> Read()
        {
            return Ok(_unitOfWork.Tasks.GetAll());
        }

        [Route("update")]
        [HttpPut]
        public ActionResult<int> Update([FromBody] DTO.Task task)
        {
            if (_unitOfWork.Tasks.Get(task.Id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Tasks.Update(task);
                return Ok(_unitOfWork.Tasks.Get(task.Id));
            }
            catch (ArgumentException e)
            {
                if (e.Message == "Vehicle not found")
                {
                    return NotFound(e.Message);
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public ActionResult<int> Delete(int id)
        {
            if (_unitOfWork.Tasks.Get(id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Tasks.Delete(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
