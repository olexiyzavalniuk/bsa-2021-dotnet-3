﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTO
{
    public class Hierarchy
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public List<Task2> Tasks { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}
