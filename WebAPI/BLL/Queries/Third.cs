﻿using System.Collections.Generic;
using WebAPI.DTO;
using WebAPI.DAL;
using System.Linq;

namespace WebAPI.BLL
{
    public static class Third
    {
        public static List<DTO.Third> Run(int id)
        {
            List<DTO.Third> result;
            result = Query.Hierarchies
               .SelectMany(h => h.Tasks.Where(t => t.Performer.Id == id && t.FinishedAt != null
               && t.FinishedAt.Value.Year == 2021))
               .Select(t => new DTO.Third()
               {
                   id = t.Id,
                   name = t.Name
               }).ToList();

            return result;
        }
    }
}
