﻿using System.Collections.Generic;
using WebAPI.DTO;
using WebAPI.DAL;
using System.Linq;

namespace WebAPI.BLL
{
    public static class Second
    {
        public static List<Task2> Run(int id)
        {
            List<Task2> result;
            result = Query.Hierarchies.Where(hierarchy => hierarchy.Tasks.Any(task => task.Performer.Id == id &&
            task.Name.Length < 45)).
                SelectMany(h => h.Tasks.Where(t => t.Performer.Id == id)).ToList();

            return result;
        }
    }
}
